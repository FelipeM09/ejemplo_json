async function leerJSON(url) {

  try {
    let response = await fetch(url);
    let user = await response.json();
    return user;
  } catch(err) {
    
    alert(err);
  }
}


function mostrar(){

  var titulo = document.getElementById("titulo");
  var url = "https://raw.githubusercontent.com/madarme/persistencia_2_2020/main/previo2_estudiantes.json?token=ACGYEGDJXZZTV6YIQ3Y5GL3ASQNEG";
    

  leerJSON(url).then(datos => {
      titulo.innerHTML = "<h1>" + datos.nombreMateria + "</h1>";

      var alumnos = datos.estudiantes;
      var des = datos.descripcion;      
     
      
      drawTable(alumnos) 
      drawTableDescripcion(des)
  })
}




  
function drawTable(datos) {
  
  var data = new google.visualization.DataTable();

  
  // CODIGO Y NOMBRE
  data.addColumn('number', 'CODIGO');
  data.addColumn('string', 'ESTUDIANTE');

  // NOTAS
  for(let i=0; i<datos[0].notas.length; i++){
     data.addColumn("number", "NOTA (id= " + (i+1) + ")");
  }
  // PROMEDIO
  data.addColumn("number", "PROMEDIO");

  // LLENAR DATOS
  data.addRows(llenarTabla(datos));

  var table = new google.visualization.Table(document.getElementById('table_div'));

  table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
}


function llenarTabla(datos){
  var arreglo = [];
  let posicion=0;

  for(let i=0; i<datos.length; i++){
    let estudiante = datos[i];
      let alumno = [];

    alumno.push(estudiante.codigo);
    alumno.push(estudiante.nombre);

    for(let j=2; j<=datos[i].notas.length+1; j++){
      alumno.push(leerNota(estudiante.notas, j-2));
           posicion++;
    }
    alumno.push(calcularPromedio(estudiante.notas));
    arreglo.push(alumno);
    posicion=0;
  }
   return arreglo;
}



function leerEstudiante(estudiante, posicion){
  return estudiante[posicion];
}

function leerNota(notas, posicion){
  let nota = notas[posicion].valor;
  return nota;
}


function calcularPromedio(notas){
    let promedio = 0;

    for(let i = 0; i<notas.length; i++){
      promedio += notas[i].valor;
    }
    return (promedio/notas.length);
}



function drawTableDescripcion(descripcion) {
  
  var data = new google.visualization.DataTable();
  data.addColumn('number', 'ID');
  data.addColumn('string', 'DESCRIPCIÓN');
  
  
  data.addRows(descripcion.length);
  for(let i = 0; i<descripcion.length; i++){
    data.setCell(i,0, descripcion[i].id);
    data.setCell(i,1, descripcion[i].descripcion);
  }

  var table = new google.visualization.Table(document.getElementById('descripcion'));

  table.draw(data, {showRowNumber: false, width: '100%', height: '100%'});
}

mostrar();